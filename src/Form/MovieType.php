<?php

namespace App\Form;

use App\Entity\Actor;
use App\Entity\Director;
use App\Entity\Genre;
use App\Entity\Movie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('post')
            ->add('title', TextType::class)
            ->add('description', TextareaType::class, ['attr' => ['rows' => 5]])
            ->add('releaseYear', NumberType::class)
            ->add(
                'genres',
                EntityType::class,
                [
                    'class' => Genre::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'expanded' => true,
                    'attr'=> [
                        'style'=>'max-height: 200px; overflow: auto;'
                    ]
                ]
            )
            ->add(
                'actors',
                EntityType::class,
                [
                    'class' => Actor::class,
                    'choice_label' => 'fullname',
                    'multiple' => true,
                    'expanded' => true,
                    'attr'=> [
                        'style'=>'max-height: 200px; overflow: auto;'
                    ]
                ]
            )
            ->add(
                'directors',
                EntityType::class,
                [
                    'class' => Director::class,
                    'choice_label' => 'fullname',
                    'multiple' => true,
                    'expanded' => true,
                    'attr'=> [
                        'style'=>'max-height: 200px; overflow: auto;'
                    ]
                ]
            )
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Movie::class,
            ]
        );
    }
}
