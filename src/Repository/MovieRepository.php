<?php

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    /**
     * @param string $searchTerm
     * @return Movie[] Returns an array of Movie objects
     */
    public function findBySearchTerm(string $searchTerm): iterable
    {
        $query = $this->createQueryBuilder('m')
            ->andWhere('m.title like :q')
            ->join('m.genres', 'genre')
            ->join('m.actors', 'actor')
            ->join('m.directors', 'director')
            ->orWhere('genre.name like :q')
            ->orWhere('actor.name like :q')
            ->orWhere('actor.surname like :q')
            ->orWhere('director.name like :q')
            ->orWhere('director.surname like :q')
            ->setParameter('q', '%'.$searchTerm.'%')
            ->getQuery();

        return $query->getResult();
    }
}
