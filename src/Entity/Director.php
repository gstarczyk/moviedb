<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="directors")
 */
class Director
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="director_id", type="integer")
     */
    private $id;

    /**
     * @var ?string
     * @ORM\Column(name="director_name", type="string", length=255)
     */
    private $name;

    /**
     * @var ?string
     * @ORM\Column(name="director_surname", type="string", length=255)
     */
    private $surname;

    /**
     * @var ?DateTimeImmutable
     * @ORM\Column(name="date_of_birth", type="date_immutable")
     */
    private $birthDate;

    /**
     * @var Movie[]
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="directors")
     */
    private $movies = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function setBirthDate(?DateTimeImmutable $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getMovies(): iterable
    {
        return $this->movies;
    }

    public function setMovies(array $movies): self
    {
        $this->movies = $movies;

        return $this;
    }

    public function getFullname(): string
    {
        return $this->name.' '.$this->surname;
    }
}
