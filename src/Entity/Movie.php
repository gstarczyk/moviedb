<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MovieRepository::class)
 * @ORM\Table(name="movies")
 */
class Movie
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="movie_id", type="integer")
     */
    private $id;

    /**
     * @var ?string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var ?int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $releaseYear;

    /**
     * @var ?string
     * @ORM\Column(name="descryption", type="text")
     */
    private $description;

    /**
     * @var Genre[]
     * @ORM\ManyToMany(targetEntity="Genre", inversedBy="movies")
     * @ORM\JoinTable(name="movies_genres",
     *      joinColumns={@ORM\JoinColumn(name="movie_id", referencedColumnName="movie_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="genre_id", referencedColumnName="genre_id")}
     *  )
     */
    private $genres = [];

    /**
     * @var Actor[]
     * @ORM\ManyToMany(targetEntity="Actor", inversedBy="movies")
     * @ORM\JoinTable(name="movies_actors",
     *      joinColumns={@ORM\JoinColumn(name="movie_id", referencedColumnName="movie_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="actor_id", referencedColumnName="actor_id")}
     * )
     */
    private $actors = [];

    /**
     * @var Director[]
     * @ORM\ManyToMany(targetEntity="Director", inversedBy="movies", cascade={"persist","remove"})
     * @ORM\JoinTable(name="movies_directors",
     *      joinColumns={@ORM\JoinColumn(name="movie_id", referencedColumnName="movie_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="director_id", referencedColumnName="director_id")}
     * )
     */
    private $directors = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getReleaseYear(): ?int
    {
        return $this->releaseYear;
    }

    public function setReleaseYear(?int $releaseYear): self
    {
        $this->releaseYear = $releaseYear;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getGenres(): iterable
    {
        return $this->genres;
    }

    public function setGenres(array $genres): self
    {
        $this->genres = $genres;

        return $this;
    }

    public function getActors(): iterable
    {
        return $this->actors;
    }

    public function setActors(array $actors): self
    {
        $this->actors = $actors;

        return $this;
    }

    public function getDirectors(): iterable
    {
        return $this->directors;
    }

    public function setDirectors(array $directors): self
    {
        $this->directors = $directors;

        return $this;
    }
}
