<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="genres")
 */
class Genre
{
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="genre_id", type="integer")
     */
    private $id;

    /**
     * @var ?string
     * @ORM\Column(name="genre_name", type="string", length=45)
     */
    private $name;

    /**
     * @var Movie[]
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="genres")
     */
    private $movies = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getMovies(): iterable
    {
        return $this->movies;
    }

    public function setMovies(array $movies): self
    {
        $this->movies = $movies;

        return $this;
    }
}
