<?php

namespace App\Controller;

use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $searchTerm = $request->get('q');
        if ($searchTerm !== null) {
            $repo = $this->getDoctrine()->getRepository(Movie::class);
            $movies = $repo->findBySearchTerm($searchTerm);
        } else {
            $movies = [];
        }

        return $this->render(
            'default/index.html.twig',
            [
                'searchTerm' => $searchTerm,
                'movies' => $movies
            ]
        );
    }
}
