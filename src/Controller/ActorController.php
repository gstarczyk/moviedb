<?php

namespace App\Controller;

use App\Entity\Actor;
use App\Form\ActorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/actors")
 */
class ActorController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function list(): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Actor[] $actors */
        $actors = $em->getRepository(Actor::class)->findAll();

        return $this->render(
            'actors/list.html.twig',
            [
                'actors' => $actors,
            ]
        );
    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @param Actor $actor
     * @return Response
     */
    public function details(Actor $actor): Response
    {
        return $this->render(
            'actors/details.html.twig',
            [
                'actor' => $actor,
            ]
        );
    }

    /**
     * @Route("/{id}/edit")
     * @param Actor $actor
     * @param Request $request
     * @return Response
     */
    public function edit(Actor $actor, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(ActorType::class, $actor);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('app_actor_details', ['id' => $actor->getId()]);
        }

        return $this->render(
            'actors/edit.html.twig',
            [
                'actor' => $actor,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $actor = new Actor();
        $form = $this->createForm(ActorType::class, $actor);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($actor);
            $entityManager->flush();

            return $this->redirectToRoute('app_actor_details', ['id' => $actor->getId()]);
        }

        return $this->render(
            'actors/edit.html.twig',
            [
                'actor' => $actor,
                'form' => $form->createView(),
            ]
        );
    }
}
