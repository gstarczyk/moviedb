<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Form\MovieType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/movies")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function list(): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Movie[] $movies */
        $movies = $em->getRepository(Movie::class)->findAll();

        return $this->render(
            'movies/list.html.twig',
            [
                'movies' => $movies,
            ]
        );
    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @param Movie $movie
     * @return Response
     */
    public function details(Movie $movie): Response
    {
        return $this->render(
            'movies/details.html.twig',
            [
                'movie' => $movie,
            ]
        );
    }

    /**
     * @Route("/create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $movie = new Movie();
        $form = $this->createForm(MovieType::class, $movie);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movie);
            $entityManager->flush();

            return $this->redirectToRoute('app_movie_details', ['id' => $movie->getId()]);
        }

        return $this->render(
            'movies/edit.html.twig',
            [
                'movie' => $movie,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}/edit")
     * @param Movie $movie
     * @param Request $request
     * @return Response
     */
    public function edit(Movie $movie, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(MovieType::class, $movie);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('app_movie_details', ['id' => $movie->getId()]);
        }

        return $this->render(
            'movies/edit.html.twig',
            [
                'movie' => $movie,
                'form' => $form->createView(),
            ]
        );
    }
}
