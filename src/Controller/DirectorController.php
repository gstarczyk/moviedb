<?php

namespace App\Controller;

use App\Entity\Director;
use App\Form\DirectorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/directors")
 */
class DirectorController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function list(): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Director[] $directors */
        $directors = $em->getRepository(Director::class)->findAll();

        return $this->render(
            'directors/list.html.twig',
            [
                'directors' => $directors,
            ]
        );
    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @param Director $director
     * @return Response
     */
    public function details(Director $director): Response
    {
        return $this->render(
            'directors/details.html.twig',
            [
                'director' => $director,
            ]
        );
    }

    /**
     * @Route("/{id}/edit")
     * @param Director $director
     * @param Request $request
     * @return Response
     */
    public function edit(Director $director, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(DirectorType::class, $director);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('app_director_details', ['id' => $director->getId()]);
        }

        return $this->render(
            'directors/edit.html.twig',
            [
                'director' => $director,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $director = new Director();
        $form = $this->createForm(DirectorType::class, $director);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($director);
            $entityManager->flush();

            return $this->redirectToRoute('app_director_details', ['id' => $director->getId()]);
        }

        return $this->render(
            'directors/edit.html.twig',
            [
                'director' => $director,
                'form' => $form->createView(),
            ]
        );
    }
}
