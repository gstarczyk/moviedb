<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Form\GenreType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/genres")
 */
class GenreController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function list(): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Genre[] $genres */
        $genres = $em->getRepository(Genre::class)->findAll();

        return $this->render(
            'genres/list.html.twig',
            [
                'genres' => $genres,
            ]
        );
    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @param Genre $genre
     * @return Response
     */
    public function details(Genre $genre): Response
    {
        return $this->render(
            'genres/details.html.twig',
            [
                'genre' => $genre,
            ]
        );
    }

    /**
     * @Route("/{id}/edit")
     * @param Genre $genre
     * @param Request $request
     * @return Response
     */
    public function edit(Genre $genre, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(GenreType::class, $genre);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('app_genre_details', ['id' => $genre->getId()]);
        }

        return $this->render(
            'genres/edit.html.twig',
            [
                'genre' => $genre,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $genre = new Genre();
        $form = $this->createForm(GenreType::class, $genre);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($genre);
            $entityManager->flush();

            return $this->redirectToRoute('app_genre_details', ['id' => $genre->getId()]);
        }

        return $this->render(
            'genres/edit.html.twig',
            [
                'genre' => $genre,
                'form' => $form->createView(),
            ]
        );
    }
}
