### Build and run local environment
    docker-compose build
    docker-compose up -d

### Setup local environment

    docker exec -u dev web bash

then

    cd /var/www/site/
    composer install
    php bin/console doctrine:database:create --if-not-exists
    php bin/console doctrine:migrations:migrate --no-interaction    

### Access GUI
Go to `http://127.0.0.1:8001/` url to access local GUI.
