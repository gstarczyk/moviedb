-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema new_movie_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema new_movie_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `new_movie_db` DEFAULT CHARACTER SET utf8 ;
USE `new_movie_db` ;

-- -----------------------------------------------------
-- Table `new_movie_db`.`movie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`movie` (
  `id_movie` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `release_year` INT NULL,
  `descryption` TEXT(500) NULL,
  PRIMARY KEY (`id_movie`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_movie_db`.`directors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`directors` (
  `id_director` INT NOT NULL,
  `director_name` VARCHAR(255) NULL,
  `director_surname` VARCHAR(255) NULL,
  PRIMARY KEY (`id_director`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_movie_db`.`movie_director`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`movie_director` (
  `movie_id` INT NOT NULL,
  `director_id` INT NOT NULL,
  PRIMARY KEY (`movie_id`, `director_id`),
  INDEX `director_ids_idx` (`director_id` ASC) VISIBLE,
  CONSTRAINT `movie_ids`
    FOREIGN KEY (`movie_id`)
    REFERENCES `new_movie_db`.`movie` (`id_movie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `director_ids`
    FOREIGN KEY (`director_id`)
    REFERENCES `new_movie_db`.`directors` (`id_director`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_movie_db`.`actors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`actors` (
  `id_actors` INT NOT NULL,
  `actor_name` VARCHAR(255) NULL,
  `actor_surname` VARCHAR(255) NULL,
  PRIMARY KEY (`id_actors`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_movie_db`.`movie_actor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`movie_actor` (
  `movie_id` INT NOT NULL,
  `actor_id` INT NOT NULL,
  PRIMARY KEY (`movie_id`, `actor_id`),
  INDEX `actor_ids_idx` (`actor_id` ASC) VISIBLE,
  CONSTRAINT `movie_ida`
    FOREIGN KEY (`movie_id`)
    REFERENCES `new_movie_db`.`movie` (`id_movie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `actor_ids`
    FOREIGN KEY (`actor_id`)
    REFERENCES `new_movie_db`.`actors` (`id_actors`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_movie_db`.`genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`genre` (
  `id_genre` INT NOT NULL,
  `genre_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id_genre`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_movie_db`.`movie_genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_movie_db`.`movie_genre` (
  `movie_id` INT NOT NULL,
  `genre_id` INT NOT NULL,
  PRIMARY KEY (`movie_id`, `genre_id`),
  INDEX `genre_uid_idx` (`genre_id` ASC) VISIBLE,
  CONSTRAINT `movie_uid`
    FOREIGN KEY (`movie_id`)
    REFERENCES `new_movie_db`.`movie` (`id_movie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `genre_uid`
    FOREIGN KEY (`genre_id`)
    REFERENCES `new_movie_db`.`genre` (`id_genre`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
