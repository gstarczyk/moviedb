ALTER TABLE `new_movie_db`.`actors` 
ADD COLUMN `date_of_birth` DATE NULL AFTER `actor_surname`;

ALTER TABLE `new_movie_db`.`directors` 
ADD COLUMN `date_of_birth` DATE NULL AFTER `director_surname`;
