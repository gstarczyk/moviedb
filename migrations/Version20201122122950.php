<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122122950 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'INSERT INTO admin_users (`username`, `password`, `roles`) '
            .'VALUES ("admin","$2y$10$xTq/0Ru8jW/SRyuc9efhquJgSexdVtSz8MERz4xV0qnrNYFcExOI6", "[\"ROLE_ADMIN\"]")'
        );
    }

    public function down(Schema $schema): void
    {

    }
}
