-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: new_movie_db
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `actors`
--

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` VALUES (1,'Mia','Wasikowska'),(2,'Jemma','Powell'),(3,'Johnny','Depp'),(4,'Helena','Carter'),(5,'Anne','Hathaway'),(6,'Jake','Gyllenhaal'),(7,'Ben','Kingsley'),(8,'Gemma','Arterton'),(9,'Alfred','Molina'),(10,'Kristen','Bell'),(11,'Josh','Duhamel'),(12,'Alexis','Dziena'),(13,'Peggy','Peggy'),(14,'Luca','Calvani'),(15,'Miley','Cyrus'),(16,'Liam','Hemsworth'),(17,'Bobby','Coleman'),(18,'Kelly','Preston'),(19,'Greg','Kinnear'),(20,'Tom','Hanks'),(21,'Tim','Allen'),(22,'Joan','Cusack'),(23,'Don','Rickles'),(24,'Wallace','Shawn'),(25,'Nicolas','Cage'),(26,'Jay','Baruchel'),(27,'Teresa','Palmer'),(28,'Monica','Bellucci');
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `directors`
--

LOCK TABLES `directors` WRITE;
/*!40000 ALTER TABLE `directors` DISABLE KEYS */;
INSERT INTO `directors` VALUES (1,'Tim','Burton'),(2,'Mike','Newell'),(3,'Mark','Johnson'),(4,'Julie','Robinson'),(5,'Lee','Unkrich'),(6,'Jon','Turteltaub');
/*!40000 ALTER TABLE `directors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'family'),(2,'fantasy'),(3,'adventure film'),(4,'action'),(5,'adventure'),(6,'romantic comedy'),(7,'romantic drama'),(8,'animation'),(9,'comedy');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'Alice in Wonderland',2010,'When Alice was a little girl, she ran after the White Rabbit, fell into his den and found herself in a fantastic and somewhat bizarre world called Wonderland, where fairy-tale creatures live, talking animals and singing flowers, and residents celebrate no birthdays. Now Alicja is 19 years old and she returns to this extraordinary land, remembering nothing from her previous visit. It comes to this when Alice runs away just before her engagement.'),(2,'Prince of Persia: The sands of the time',2010,'The youngest, adopted son of the King of Persia, Sharaman, the brave Prince of Dastan has no equal in battle. After the attack on the holy city of Alamut (accused of supplying Persia\'s enemies with weapons), Dastan acquires the Dagger of Time. Dastan\'s father, in order to calm the inhabitants of the captured city, proposes a wedding of his foster son with the city\'s ruler - Princess Tamina.'),(3,'When in Rome',2010,'Unlucky in love Beth (Kristen Bell), a successful New York curator, travels to Rome for her sister\'s wedding. There, she falls in love with one of the guests, the charming reporter Nick (Josh Duhamel), but it seems that her infatuation remains unrequited. In an act of rebellion, a disappointed Beth takes coins from the Roman fountain of love, unaware of the magical consequences of her act.'),(4,'The last song',2010,'The main character, Ronnie, cannot come to terms with her parents\' divorce. Her father, Steve, tries to rebuild the loving relationship that once existed between him and his daughter, but reconnecting with a rebellious girl is not easy. He decides to use the only thing that still connects them - music. Meanwhile, Ronnie meets the handsome beach volleyball player Will, with whom she has her first love.'),(5,'Toy Story 3',2010,'Andy becomes a grown man. He graduates from high school and goes to college. time to say goodbye to your toys. The boy cannot leave his favorite Cowboy. His mother decides to give the rest of his toys to a nearby kindergarten. Ultimately, they end up in the hands of riotous kids. According to the motto \"one for all, all for one\", the toys decide to support each other and plan a spectacular escape. They make friends with newly met plush friends and set out on an adventurous journey with them.'),(6,'The Sorcerers Apprentice',2010,'Balthazar Blake is a master sorcerer in modern-day Manhattan trying to defend the city from his arch-nemesis, Maxim Horvath. Balthazar can\'t do it alone, so he recruits Dave Stutler, a seemingly average guy who demonstrates hidden potential, as his reluctant protégé. The sorcerer gives his unwilling accomplice a crash course in the art and science of magic, and together, these unlikely partners work to stop the forces of darkness.');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `movie_actor`
--

LOCK TABLES `movie_actor` WRITE;
/*!40000 ALTER TABLE `movie_actor` DISABLE KEYS */;
INSERT INTO `movie_actor` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(2,6),(2,7),(2,8),(2,9),(6,9),(3,10),(3,11),(3,12),(3,13),(3,14),(4,15),(4,16),(4,17),(4,18),(4,19),(5,20),(5,21),(5,22),(5,23),(5,24),(6,25),(6,26),(6,27),(6,28);
/*!40000 ALTER TABLE `movie_actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `movie_director`
--

LOCK TABLES `movie_director` WRITE;
/*!40000 ALTER TABLE `movie_director` DISABLE KEYS */;
INSERT INTO `movie_director` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6);
/*!40000 ALTER TABLE `movie_director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `movie_genre`
--

LOCK TABLES `movie_genre` WRITE;
/*!40000 ALTER TABLE `movie_genre` DISABLE KEYS */;
INSERT INTO `movie_genre` VALUES (1,1),(1,2),(2,2),(5,2),(1,3),(2,4),(5,4),(2,5),(5,5),(6,5),(3,6),(4,7),(6,8),(6,9);
/*!40000 ALTER TABLE `movie_genre` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-02  7:44:55
