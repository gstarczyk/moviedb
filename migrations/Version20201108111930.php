<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201108111930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'create initial DB structure';
    }

    public function up(Schema $schema) : void
    {
        $sql = file_get_contents(__DIR__.'/Version20201108111930.sql');
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // there is no going back!
    }
}
