ALTER TABLE `new_movie_db`.`movie_genre`
    DROP FOREIGN KEY `movie_uid`,
    DROP FOREIGN KEY `genre_uid`;
ALTER TABLE `new_movie_db`.`movie_genre`
    DROP INDEX `genre_uid_idx`;

ALTER TABLE `new_movie_db`.`movie_genre`
    RENAME TO `new_movie_db`.`movies_genres`;

ALTER TABLE `new_movie_db`.`movie_actor`
    DROP FOREIGN KEY `movie_ida`,
    DROP FOREIGN KEY `actor_ids`;
ALTER TABLE `new_movie_db`.`movie_actor`
    DROP INDEX `actor_ids_idx`;

ALTER TABLE `new_movie_db`.`movie_actor`
    RENAME TO `new_movie_db`.`movies_actors`;

ALTER TABLE `new_movie_db`.`movie_director`
    DROP FOREIGN KEY `movie_ids`,
    DROP FOREIGN KEY `director_ids`;
ALTER TABLE `new_movie_db`.`movie_director`
    DROP INDEX `director_ids_idx`;

ALTER TABLE `new_movie_db`.`movie_director`
    RENAME TO `new_movie_db`.`movies_directors`;

ALTER TABLE `new_movie_db`.`movie`
    CHANGE COLUMN `id_movie` `movie_id` INT NOT NULL AUTO_INCREMENT,
    ADD INDEX `m_title` (`title` ASC) VISIBLE,
    ADD INDEX `m_release_year` (`release_year` ASC) VISIBLE;

ALTER TABLE `new_movie_db`.`movie`
    RENAME TO `new_movie_db`.`movies`;

ALTER TABLE `new_movie_db`.`genre`
    CHANGE COLUMN `id_genre` `genre_id` INT NOT NULL AUTO_INCREMENT,
    ADD INDEX `g_name` (`genre_name` ASC) VISIBLE;

ALTER TABLE `new_movie_db`.`genre`
    RENAME TO `new_movie_db`.`genres`;

ALTER TABLE `new_movie_db`.`directors`
    CHANGE COLUMN `id_director` `director_id` INT NOT NULL AUTO_INCREMENT,
    ADD INDEX `d_name` (`director_name` ASC) VISIBLE,
    ADD INDEX `d_surname` (`director_surname` ASC) VISIBLE;

ALTER TABLE `new_movie_db`.`actors`
    CHANGE COLUMN `id_actors` `actor_id` INT NOT NULL AUTO_INCREMENT,
    ADD INDEX `a_name` (`actor_name` ASC) VISIBLE,
    ADD INDEX `a_surname` (`actor_surname` ASC) VISIBLE;

ALTER TABLE `new_movie_db`.`movies_directors`
    ADD INDEX `director_id_idx` (`director_id` ASC) VISIBLE;

ALTER TABLE `new_movie_db`.`movies_directors`
    ADD CONSTRAINT `movie_id`
        FOREIGN KEY (`movie_id`)
            REFERENCES `new_movie_db`.`movies` (`movie_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    ADD CONSTRAINT `director_id`
        FOREIGN KEY (`director_id`)
            REFERENCES `new_movie_db`.`directors` (`director_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;

ALTER TABLE `new_movie_db`.`movies_actors`
    ADD CONSTRAINT `actor_id`
        FOREIGN KEY (`actor_id`)
            REFERENCES `new_movie_db`.`actors` (`actor_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    ADD CONSTRAINT `movie_ids`
        FOREIGN KEY (`movie_id`)
            REFERENCES `new_movie_db`.`movies` (`movie_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;

ALTER TABLE `new_movie_db`.`movies_genres`
    ADD INDEX `genre_id_idx` (`genre_id` ASC) VISIBLE;

ALTER TABLE `new_movie_db`.`movies_genres`
    ADD CONSTRAINT `genre_id`
        FOREIGN KEY (`genre_id`)
            REFERENCES `new_movie_db`.`genres` (`genre_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    ADD CONSTRAINT `movie_uid`
        FOREIGN KEY (`movie_id`)
            REFERENCES `new_movie_db`.`movies` (`movie_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
