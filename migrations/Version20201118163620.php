<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201118163620 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Refine DB indexes';
    }

    public function up(Schema $schema) : void
    {
        $sql = file_get_contents(__DIR__.'/Version20201118163620.sql');
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // there is no going back!
    }
}
