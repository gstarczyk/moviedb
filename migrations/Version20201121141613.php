<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201121141613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql = file_get_contents(__DIR__.'/Version20201121141613.sql');
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // there is no going back!
    }
}
